﻿using System;
using System.Collections.Generic;
using MovieMonkey.ViewModels;
using Xamarin.Forms.Xaml;
using MovieApi.Services;

using Xamarin.Forms;

namespace MovieMonkey
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TopRatedPage : ContentPage
    {
        MovieListViewModel _movieListViewModel;
        MovieService _service;
        MovieViewContext _viewContext;

        public TopRatedPage(MovieViewContext viewContext, MovieService movieService)
        {
            _service = movieService;
            _viewContext = viewContext;
            PopulateList();
        }

        private async void PopulateList()
        {
            var topRatedMovies = await _service.GetTopRated();
            _viewContext.SetTopRatedMovies(topRatedMovies);
            _movieListViewModel = new MovieListViewModel(this.Navigation, _viewContext.TopRatedMovies);
            this.BindingContext = _movieListViewModel;
            InitializeComponent();
        }

        public async void PopulateActors()
        {
            foreach (MovieViewModel movie in _movieListViewModel.Movies)
            {
                var movieId = movie.MovieId;
                movie.Actors = await _service.GetCastByMovieId(movieId);
            }
        }

    }
}

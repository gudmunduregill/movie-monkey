﻿using System;
using System.Collections.Generic;
using MovieMonkey.ViewModels;
using Xamarin.Forms.Xaml;
using MovieApi.Services;

using Xamarin.Forms;

namespace MovieMonkey
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopularMoviesPage : ContentPage
    {
        MovieListViewModel _movieListViewModel;
        MovieService _service;
        MovieViewContext _viewContext;

        public PopularMoviesPage(MovieViewContext viewContext, MovieService movieService)
        {
            _service = movieService;
            _viewContext = viewContext;
            PopulateList();
        }

        private async void PopulateList()
        {
            var popularMovies = await _service.GetPopular();
            _viewContext.SetPopularMovies(popularMovies);
            _movieListViewModel = new MovieListViewModel(this.Navigation, _viewContext.PopularMovies);
            this.BindingContext = _movieListViewModel;
            InitializeComponent();
        }

    }
}
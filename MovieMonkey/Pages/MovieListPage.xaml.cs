﻿using MovieMonkey.ViewModels;
using MovieApi.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovieMonkey
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MovieListPage : ContentPage
    {
        MovieListViewModel _movieListViewModel;
        MovieService _movieService;
        public MovieListPage(MovieViewContext viewContext)
        {
            _movieService = new MovieService();
            _movieListViewModel = new MovieListViewModel(this.Navigation, viewContext.MoviesSearchedFor);
            this.BindingContext = _movieListViewModel;
            InitializeComponent();
            PopulateActors();
        }

        private async void PopulateActors()
        {
            foreach (MovieViewModel movie in _movieListViewModel.Movies)
            {
                var movieId = movie.MovieId;
                movie.Actors = await _movieService.GetCastByMovieId(movieId);
            }
        }

        //private bool _isRefreshing = false;
        //public bool IsRefreshing
        //{
        //    get { return _isRefreshing; }
        //    set
        //    {
        //        _isRefreshing = value;
        //        OnPropertyChanged(nameof(IsRefreshing));
        //    }
        //}

        //public Command RefreshCommand
        //{
        //    get
        //    {
        //        return new Command(async () =>
        //        {
        //            IsRefreshing = true;
		//
        //            foreach (MovieViewModel movie in _movieListViewModel.Movies)
        //            {
        //                var movieId = movie.MovieId;
        //                movie.Actors = await _movieService.GetCastByMovieId(movieId);
        //            }
		//
        //            IsRefreshing = false;
        //        });
        //    }
        //}

    }
}

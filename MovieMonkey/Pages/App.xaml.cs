﻿using Xamarin.Forms;
using MovieApi.Services;
using System.Collections.Generic;
using MovieApi.Models;
using System.Threading.Tasks;
using MovieMonkey.ViewModels;

namespace MovieMonkey
{
    public partial class App : Application
    {

        static Color BEIGE = Color.FromHex("#fadcaa");
        static Color RED = Color.FromHex("#c84b4b");
        MovieViewContext _viewContext;
        MovieService _movieService;

        public App()
        {
            InitializeComponent();
            _viewContext = new MovieViewContext();
            var tabbedPage = new TabbedPage();
            tabbedPage.BackgroundColor = BEIGE;
            tabbedPage.BarBackgroundColor = BEIGE;
            tabbedPage.BarTextColor = RED;

            _movieService = new MovieService();
            // Greeting Page (Search Page)
            var rootNavPage = new NavigationPage(new MovieMonkeyPage(_viewContext, _movieService))
            {
                Title = "Search",
                BackgroundColor = BEIGE,
                BarBackgroundColor = BEIGE,
                BarTextColor = RED
            };
            tabbedPage.Children.Add(rootNavPage);

            // Top Rated Movies Page
            var topRatedNavPage = new NavigationPage(new TopRatedPage(_viewContext, _movieService))
            {
                Title = "Top Rated",
                BackgroundColor = BEIGE,
                BarBackgroundColor = BEIGE,
                BarTextColor = RED
            };
            tabbedPage.Children.Add(topRatedNavPage);

            // Top Rated Movies Page
            var popularNavPage = new NavigationPage(new PopularMoviesPage(_viewContext, _movieService))
            {
                Title = "Popular",
                BackgroundColor = BEIGE,
                BarBackgroundColor = BEIGE,
                BarTextColor = RED
            };
            tabbedPage.Children.Add(popularNavPage);

            this.MainPage = tabbedPage;

            //tabbedPage.CurrentPageChanged += (object sender, System.EventArgs e) => {
            //    var list = tabbedPage.Children;
            //};
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

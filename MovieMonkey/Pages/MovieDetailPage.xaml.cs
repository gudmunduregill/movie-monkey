﻿using System;
using System.Collections.Generic;
using MovieMonkey.ViewModels;
using MovieApi.Models;
using MovieApi.Services;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace MovieMonkey
{
    public partial class MovieDetailPage : ContentPage
    {
        private MovieViewModel _movie;
        private MovieService _service;
        private int _movieId;

        public MovieDetailPage(MovieViewModel movie)
        {
            _service = new MovieService();
            _movie = movie;
            this.BindingContext = _movie;
            InitializeComponent();
            PopulateExtraInfo();
        }

        private async void PopulateExtraInfo()
        {
            var extraInfo = await _service.GetExtraInfoByMovieId(_movie.MovieId);
            _movie.HeaderSummary = extraInfo.Runtime + " | " + extraInfo.Genres;
            _movie.Tagline = extraInfo.Tagline;
        }
    }
}

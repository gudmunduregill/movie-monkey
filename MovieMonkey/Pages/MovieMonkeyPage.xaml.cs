﻿using Xamarin.Forms;
using MovieApi.Services;
using MovieApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using MovieMonkey.ViewModels;

namespace MovieMonkey
{
    public partial class MovieMonkeyPage : ContentPage
    {

        MovieService _service;
        MovieViewContext _viewContext;
        MovieListViewModel _movieListViewModel;

        public MovieMonkeyPage(MovieViewContext viewContext, MovieService service)
        {
            _service = service;
            _viewContext = viewContext;
            this.InitializeComponent();
            AttachEvents();
        }

        async void AttachEvents()
        {
            search_bar.SearchButtonPressed += async (sender, e) => {
                var searchTerm = ((SearchBar)sender).Text;
                var movies = await _service.GetMoviesByTitle(searchTerm);
                _viewContext.SetMoviesSearchedFor(movies);
                _movieListViewModel = new MovieListViewModel(this.Navigation, _viewContext.MoviesSearchedFor);
                this.BindingContext = _movieListViewModel;
                PopulateActors();
            };
        }

        private async void PopulateActors()
        {
            foreach (MovieViewModel movie in _movieListViewModel.Movies)
            {
                var movieId = movie.MovieId;
                movie.Actors = await _service.GetCastByMovieId(movieId);
            }
        }

        //async void Search(object sender, System.EventArgs e)
        //{
        //    loading_wheel.IsRunning = true;
        //    movie_search_button.IsVisible = false;
        //    _moviesSearchedFor = await _service.GetMoviesByTitle(movie_name_input.Text);
		//
        //    _viewContext.SetMoviesSearchedFor(_moviesSearchedFor);
		//
        //    await this.Navigation.PushAsync(new MovieListPage(_viewContext));
        //    _movieListViewModel = new MovieListViewModel(this.Navigation, _viewContext.MoviesSearchedFor);
		//
        //    loading_wheel.IsRunning = false;
        //    movie_search_button.IsVisible = true;
        //}

    }
}

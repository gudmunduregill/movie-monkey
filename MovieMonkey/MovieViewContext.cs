﻿using System;
using MovieMonkey.ViewModels;
using System.Collections.Generic;
using MovieApi.Models;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MovieMonkey
{
    public class MovieViewContext : INotifyPropertyChanged
    {
        private List<MovieViewModel> _moviesSearchedFor;
        private List<MovieViewModel> _topRatedMovies;
        private List<MovieViewModel> _popularMovies;

        public MovieViewContext()
        {
            _moviesSearchedFor = new List<MovieViewModel>();
            _topRatedMovies = new List<MovieViewModel>();
            _popularMovies = new List<MovieViewModel>();
        }

        public MovieViewContext(List<MovieDataModel> movies)
        {
            _moviesSearchedFor = new List<MovieViewModel>();
            foreach (MovieDataModel movie in movies)
            {
                _moviesSearchedFor.Add(new MovieViewModel(movie));
            }
        }

        public void SetMoviesSearchedFor(List<MovieDataModel> data)
        {
            SetData(data, _moviesSearchedFor);
        }

        public void SetTopRatedMovies(List<MovieDataModel> data)
        {
            SetData(data, _topRatedMovies);
        }

        public void SetPopularMovies(List<MovieDataModel> data)
        {
            SetData(data, _popularMovies);
        }

        private void SetData(List<MovieDataModel> data, List<MovieViewModel> movieList)
        {
            foreach (var movie in data)
            {
                movieList.Add(new MovieViewModel(movie));
            }
        }

        public List<MovieViewModel> MoviesSearchedFor {
            get => this._moviesSearchedFor;

            set
            {
                if (value != null)
                {
                    this._moviesSearchedFor = value;
                    OnPropertyChanged();
                }
            }
        } 

        public List<MovieViewModel> TopRatedMovies {
            get => this._topRatedMovies;

            set
            {
                if (value != null)
                {
                    this._topRatedMovies = value;
                    OnPropertyChanged();
                }
            }
        } 

        public List<MovieViewModel> PopularMovies {
            get => this._popularMovies;

            set
            {
                if (value != null)
                {
                    this._popularMovies = value;
                    OnPropertyChanged();
                }
            }
        } 

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

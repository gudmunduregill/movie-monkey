﻿using System;
using MovieApi.Models;
namespace MovieMonkey.ViewModels
{
    public class MovieExtraInfoView
    {
        public String Runtime { get; set; }
        public String Genres { get; set; }
        public String Tagline { get; set; }

        public MovieExtraInfoView(MovieExtraInfoDataModel movie)
        {
            Runtime = movie.Runtime;
            Genres = movie.Genres;
            Tagline = movie.Tagline;
        }
    }
}

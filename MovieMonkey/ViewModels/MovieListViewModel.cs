﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;
using MovieMonkey;

namespace MovieMonkey.ViewModels
{
    public class MovieListViewModel : INotifyPropertyChanged
    {
        private INavigation _navigation;
        private List<MovieViewModel> _movieList;
        private MovieViewModel _selectedMovie;

        public MovieListViewModel(INavigation navigation, List<MovieViewModel> movieList)
        {
            this._navigation = navigation;
            this._movieList = movieList;
        }

        public List<MovieViewModel> Movies
        {
            get => this._movieList;

            set 
            {
                this._movieList = value;
                OnPropertyChanged();
            }
        }

        public MovieViewModel SelectedMovie
        {
            get => this._selectedMovie;

            set
            {
                if (value != null)
                {
                    this._selectedMovie = value;
                    this._navigation.PushAsync(new MovieDetailPage(this._selectedMovie));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System;
using MovieApi.Models;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MovieMonkey.ViewModels
{
    public class MovieViewModel : INotifyPropertyChanged
    {
        const string BASEPATH = "http://image.tmdb.org/t/p/original";
        public MovieViewModel(){}

        public MovieViewModel(MovieDataModel data)
        {
            MovieId = data.MovieId;
            Title = data.Title;
            ReleaseYear = data.ReleaseYear;
            ReleaseDate = data.ReleaseDate;
            AverageRating = data.AverageRating;
            RemoteImageUrl = BASEPATH + data.RemoteImageUrl;
            BackDropUrl = BASEPATH + data.BackDropUrl;
            Overview = data.Overview;
        }

        public int MovieId { get; set; }
        public string Title { get; set; }
        public int ReleaseYear { get; set; }
        public DateTime ReleaseDate { get; set; }
        public double AverageRating { get; set; }
        public string RemoteImageUrl { get; set; }
        public string BackDropUrl { get; set; }
        public string Overview { get; set; }


        private string _tagline;
        private string _headerSummary;
        private string _actors;

        public String HeaderSummary
        {
            get => this._headerSummary;

            set
            {
                if (value != null)
                {
                    this._headerSummary = value;
                    OnPropertyChanged();
                }
            }
        }

        public String Tagline
        {
            get => this._tagline;

            set
            {
                if (value != null)
                {
                    this._tagline = value;
                    OnPropertyChanged();
                }
            }
        }

        public String Actors
        {
            get => this._actors;

            set
            {
                if (value != null)
                {
                    this._actors = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

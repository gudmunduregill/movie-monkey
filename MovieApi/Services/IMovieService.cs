﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MovieApi.Models;

namespace MovieApi.Services
{
    public interface IMovieService
    {
        Task<List<MovieDataModel>> GetMoviesByTitle(string title);
        List<MovieDataModel> GetMovieList();
    }
}

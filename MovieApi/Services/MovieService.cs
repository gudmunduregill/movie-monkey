﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MovieApi.Models;
using System.Linq;
using DM.MovieApi;
using DM.MovieApi.MovieDb.Movies;
using DM.MovieApi.ApiResponse;

namespace MovieApi.Services
{

    public class MovieDbSettings : IMovieDbSettings
    {
        public string ApiKey => "16e36126c564599974caa0b76f89e251";
        public string ApiUrl => "http://api.themoviedb.org/3/";
    }

    public class MovieService : IMovieService
    {
        private IApiMovieRequest _movieDbApi;
        private List<MovieDataModel> _movies;

        public MovieService()
        {
            MovieDbFactory.RegisterSettings(new MovieDbSettings());
            _movieDbApi = MovieDbFactory.Create<IApiMovieRequest>().Value;
            _movies = new List<MovieDataModel>();
        }

        public async Task<string> GetCastByMovieId(int movieId)
        {
            var actorList = new List<string>();
            var credits = await _movieDbApi.GetCreditsAsync(movieId);
            var actors = credits.Item.CastMembers;
            foreach (var actor in actors.Take(3))
            {
                actorList.Add(actor.Name);
            }
            return string.Join(", ", actorList.ToArray());
        }

        public async Task<List<MovieDataModel>> GetTopRated()
        {
            ApiSearchResponse<MovieInfo> response = await _movieDbApi.GetTopRatedAsync();
            if (response.Results == null)
            {
                return null;
            }
            foreach (MovieInfo movie in response.Results)
            {
                MovieDataModel movieToAdd = new MovieDataModel()
                {
                    MovieId = movie.Id,
                    Title = movie.Title,
                    ReleaseYear = movie.ReleaseDate.Year,
                    ReleaseDate = movie.ReleaseDate,
                    RemoteImageUrl = movie.PosterPath,
                    AverageRating = movie.VoteAverage,
                    LocalImageUrl = "icon.png",
                    BackDropUrl = movie.BackdropPath,
                    Overview = movie.Overview
                };
                _movies.Add(movieToAdd);
            }
            return _movies;
        }

        public async Task<List<MovieDataModel>> GetPopular()
        {
            ApiSearchResponse<MovieInfo> response = await _movieDbApi.GetPopularAsync(1, "en");
            if (response.Results == null)
            {
                return null;
            }
            foreach (MovieInfo movie in response.Results)
            {
                MovieDataModel movieToAdd = new MovieDataModel()
                {
                    MovieId = movie.Id,
                    Title = movie.Title,
                    ReleaseYear = movie.ReleaseDate.Year,
                    ReleaseDate = movie.ReleaseDate,
                    RemoteImageUrl = movie.PosterPath,
                    AverageRating = movie.VoteAverage,
                    LocalImageUrl = "icon.png",
                    BackDropUrl = movie.BackdropPath,
                    Overview = movie.Overview
                };
                _movies.Add(movieToAdd);
            }
            return _movies;
        }

        public async Task<List<MovieDataModel>> GetMoviesByTitle(string title)
        {
            ApiSearchResponse<MovieInfo> response = await _movieDbApi.SearchByTitleAsync(title);
            _movies = new List<MovieDataModel>();
            if (response.Results == null)
            {
                return null;
            }

            foreach (MovieInfo movie in response.Results)
            {
                MovieDataModel movieToAdd = new MovieDataModel()
                {
                    MovieId = movie.Id,
                    Title = movie.Title,
                    ReleaseYear = movie.ReleaseDate.Year,
                    ReleaseDate = movie.ReleaseDate,
                    RemoteImageUrl = movie.PosterPath,
                    AverageRating = movie.VoteAverage,
                    LocalImageUrl = "icon.png",
                    BackDropUrl = movie.BackdropPath,
                    Overview = movie.Overview
                };
                _movies.Add(movieToAdd);
            }
            return _movies;
        }

        public List<MovieDataModel> GetMovieList()
        {
            return _movies;
        }

        public async Task<MovieExtraInfoDataModel> GetExtraInfoByMovieId(int movieId)
        {
            ApiQueryResponse<Movie> response = await _movieDbApi.FindByIdAsync(movieId, "en");
            MovieExtraInfoDataModel extraInfo = new MovieExtraInfoDataModel(response.Item);
            return extraInfo;
        }

    }
}

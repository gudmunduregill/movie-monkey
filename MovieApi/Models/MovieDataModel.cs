﻿using System;
using System.Collections.Generic;
namespace MovieApi.Models
{
    public class MovieDataModel
    {
        public int MovieId { get; set; }
        public String Title { get; set; }
        public int ReleaseYear { get; set; }
        public DateTime ReleaseDate { get; set; }
        public Double AverageRating { get; set; }
        public String RemoteImageUrl { get; set; }
        public String LocalImageUrl { get; set; }
        public String Actors { get; set; }
        public String BackDropUrl { get; set; }
        public String Overview { get; set; }
    }
}

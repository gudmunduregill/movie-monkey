﻿using System;
using System.Collections.Generic;
using DM.MovieApi.MovieDb.Movies;
namespace MovieApi.Models
{
    public class MovieExtraInfoDataModel
    {
        public string Runtime { get; set; }
        public string Genres { get; set; }
        public string Tagline { get; set; }

        public MovieExtraInfoDataModel(Movie movie)
        {
            Runtime = movie.Runtime.ToString();
            Genres = string.Join(", ", movie.Genres);
            Tagline = movie.Tagline.ToString();
        }
    }
}